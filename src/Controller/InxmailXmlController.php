<?php

namespace Drupal\inxmail_xml\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\inxmail_xml\Entity\TemplateEntity;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InxmailXmlController.
 *
 * @package Drupal\
 */
class InxmailXmlController extends ControllerBase {

  /**
   * SnsNotificationSubscriber constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory) {
    $this->logger = $logger_factory->get('inxmail_xml');
    $this->config = $config_factory->getEditable('inxmail_xml.settings');
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('config.factory')
    );
  }

  /**
   * Returns a XML response.
   */
  public function xml() {
    if ($this->config->get('debug_mode')) {
      $this->logger->info('Start to generate the XML.');
    }

    $current_path = \Drupal::service('path.current')->getPath();
    $templateEntityId = basename($current_path, '.xml');
    $templateEntity = TemplateEntity::load($templateEntityId);

    // $config = \Drupal::configFactory()->getEditable('inxmail_xml.settings');

    $xmlContent = '';
    $splittedTemplate = preg_split("/({{.*?}})/", $templateEntity->template, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

    foreach ($splittedTemplate as $templatesFragment) {
      if (str_starts_with($templatesFragment, '{{')) {
        $xmlContent .= $this->getDynamicContent($templatesFragment);
      }
      else {
        $xmlContent .= $templatesFragment;
      }
    }

    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml; charset=utf-8');
    $response->setContent('<?xml version="1.0" encoding="UTF-8"?>' .
      $xmlContent
    );
    return $response;
  }

  /**
   * Get dynamic content of a fragment of the template.
   *
   * @param string $fragment
   *   Fragment of the template. The fragment has to be in the format:
   *   {{'key':'value','key':'value','key':'value'}}.
   *
   * @return string
   *   Returns dynamic content of a fragment of the template.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function getDynamicContent($fragment) {
    if ($this->config->get('debug_mode')) {
      $this->logger->info('Get the dynamic content for the following fragment:' . $fragment);
    }

    // Transform the fragment to object.
    $fragment = str_replace('{{', '{', $fragment);
    $fragment = str_replace('}}', '}', $fragment);
    $fragmentObject = json_decode($fragment);

    if (property_exists($fragmentObject, 'task')) {
      switch ($fragmentObject->task) {
        case 'list_of_nodes':
          return $this->getListOfNodes($fragmentObject);

        case 'list_of_nodes_with_day_limits':
          return $this->getListOfNodesWithDayLimits($fragmentObject);
      }
    }
    return '';
  }

  /**
   * Get list of nodes.
   *
   * @param object $fragment
   *   Fragment for which the dynamic content should be generated.
   *
   * @return string
   *   Returns list of nodes.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function getListOfNodes($fragment) {
    if ($this->config->get('debug_mode')) {
      $this->logger->info('The fragment in getListOfNodes(): <pre><code>' . print_r($fragment, TRUE) . '</code></pre>');
    }

    $content_type = $this->getFragmentValue($fragment, 'content_type', 'articles');
    $sort_field = $this->getFragmentValue($fragment, 'sort_field', 'created');
    $sort_direction = $this->getFragmentValue($fragment, 'sort_direction', 'DESC');
    $list_length = $this->getFragmentValue($fragment, 'list_length', 10);

    $nids = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', $content_type)
      ->sort($sort_field, $sort_direction)
      ->range(0, $list_length)
      ->execute();

    if ($this->config->get('debug_mode')) {
      $this->logger->info('NIDs: <pre><code>' . print_r($nids, TRUE) . '</code></pre>');
    }

    $nodes = Node::loadMultiple($nids);

    $paragraphAd = '';
    foreach ($nodes as $node) {
      $paragraphAd .= '%%lt%%a href="' .
        $node->toUrl()->setAbsolute()->toString() . '"/%%gt%%' .
        $node->getTitle() . '%%lt%%/a%%gt%%%%lt%%br%%gt%%';

    }
    return htmlspecialchars($paragraphAd);
  }

  /**
   * Get list of nodes with day limits and taxonomy.
   *
   * @param object $fragment
   *   Fragment for which the dynamic content should be generated.
   *
   * @return string
   *   Returns list of nodes with day limits and taxonomy.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getListOfNodesWithDayLimits($fragment) {
    if ($this->config->get('debug_mode')) {
      $this->logger->info('The fragment in getListOfNodesWithDayLimits(): <pre><code>' . print_r($fragment, TRUE) . '</code></pre>');
    }

    $day = (int) date('j');
    $content_type = $this->getFragmentValue($fragment, 'content_type', 'articles');
    $start_day = $this->getFragmentValue($fragment, 'start_day', 10);
    $end_day = $this->getFragmentValue($fragment, 'end_day', 20);
    $date_field = $this->getFragmentValue($fragment, 'date_field', 'field_date');
    $taxonomy_fields = $this->getFragmentValue($fragment, 'taxonomy_fields', ['field_tags']);

    $startDate = date('Y-m-' . $start_day . 'T00:00:00', strtotime('-' . $start_day . ' days'));
    $endDate = date('Y-m-' . $end_day . 'T00:00:00', strtotime('-' . $start_day . ' days'));
    if ($day >= $start_day && $day <= $end_day) {
      $startDate = date('Y-m-' . $end_day . 'T00:00:00', strtotime('-' . $end_day . ' days'));
      $endDate = date('Y-m-' . $start_day . 'T00:00:00');
    }
    if ($day > $end_day && $day <= 31) {
      $startDate = date('Y-m-' . $start_day . 'T00:00:00');
      $endDate = date('Y-m-' . $end_day . 'T00:00:00');
    }

    $nids = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', $content_type)
      ->condition($date_field, $startDate, '>=')
      ->condition($date_field, $endDate, '<')
      ->execute();

    if ($this->config->get('debug_mode')) {
      $this->logger->info('NIDs: <pre><code>' . print_r($nids, TRUE) . '</code></pre>');
    }

    $nodes = Node::loadMultiple($nids);

    $paragraphAd = '';
    $listItems = [];
    $allTerms = [];

    // Get an array with terms.
    foreach ($nodes as $node) {
      $terms = [];
      foreach ($taxonomy_fields as $taxonomy_field) {
        if (!$node->{$taxonomy_field}->isEmpty()) {
          foreach ($node->{$taxonomy_field} as $termItem) {
            $term = Term::load($termItem->getString());
            $termName = (isset($term)) ? $term->getName() : '';
            if ($termName != '') {
              $terms[] = $termName;
            }
          }
        }
      }

      foreach ($terms as $term) {
        $listItems[] = [
          'term' => $term,
          'link' => '[%url:unique-count; "' . $node->toUrl()->setAbsolute()->toString() . '"; "' . $node->title->getString() . '"]%%lt%%br/%%gt%%'
        ];
        if (!in_array($term, $allTerms)) {
          $allTerms[] = $term;
        }
      }
    }

    // Generate text, where every term is followed by all links to nodes from this term.
    $firstTerm = TRUE;
    foreach ($allTerms as $term) {
      // Add <br> before term, is it is not the first term.
      if ($firstTerm) {
        $firstTerm = FALSE;
      }
      else {
        $paragraphAd .= '%%lt%%br/%%gt%%';
      }
      // Add taxonomy term as title.
      $paragraphAd .= '%%lt%%b%%gt%%' . $term . '%%lt%%/b%%gt%%%%lt%%br/%%gt%%';
      // Add items.
      foreach ($listItems as $item) {
        if ($item['term'] == $term) {
          $paragraphAd .= $item['link'];
        }
      }
    }

    return htmlspecialchars($paragraphAd);
  }

  /**
   * Get value of a fragment property.
   *
   * @param object $fragment
   *   From the fragment the property value will be extracted.
   * @param string $property
   *   Fragment's property, which should be extracted.
   * @param mixed $default_value
   *   Default value, if the property does not exist.
   *
   * @return mixed
   *   Returns value of a fragment property.
   */
  private function getFragmentValue($fragment, $property, $default_value) {
    if (property_exists($fragment, $property) && isset($fragment->{$property})) {
      return $fragment->{$property};
    }
    else {
      return $default_value;
    }
  }

}
