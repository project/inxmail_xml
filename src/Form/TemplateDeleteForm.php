<?php

namespace Drupal\inxmail_xml\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Template entity delete form for inxmail_xml.
 */
class TemplateDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the template %id from the control list?', ['%id' => $this->entity->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.inxmail_xml_template');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the template and display the status message.
    try {
      $this->entity->delete();
      $this->logger('inxmail_xml')->info('The template %id was deleted successfully.', ['%id' => $this->entity->id()]);
      \Drupal::messenger()->addStatus($this->t('The template %id was deleted successfully.', ['%id' => $this->entity->id()]));
    }
    catch (EntityStorageException $exception) {
      $this->logger('inxmail_xml')->error('The template %id was not deleted.', ['%id' => $this->entity->id()]);
      \Drupal::messenger()->addError($this->t('The template %id was not deleted.', ['%id' => $this->entity->id()]));
    }
    // Set form redirection.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
