<?php

namespace Drupal\inxmail_xml\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;

/**
 * Template entity form for inxmail_xml.
 */
class TemplateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Prepare the form.
    $form = parent::form($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the template'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->entity->name,
      '#description' => $this->t('The name is used to generate the ID. The ID is used in the URL of the XML.'),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['name'],
      ],
    ];

    $form['template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Template'),
      '#required' => TRUE,
      '#default_value' => $this->entity->template,
      '#description' => $this->t('Define the XML code of the template.'),
    ];

    // Return the form.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $clearCache = $this->entity->isNew();
    // Save the template and display the status message.
    try {
      $this->entity->save();
    }
    catch (EntityStorageException $exception) {
      $this->logger('inxmail_xml')->error('The template %id was not saved.', ['%id' => $this->entity->id()]);
      $this->messenger()->addError($this->t('The template %id was not saved.', ['%id' => $this->entity->id()]));
    }
    // Clear the route cache.
    if ($clearCache) {
      \Drupal::service('router.builder')->rebuild();
      $this->logger('inxmail_xml')->info('The route cache has been cleared.');
    }
    // Redirect to the proper url.
    $form_state->setRedirect('entity.inxmail_xml_template');
  }

  /**
   * Checks if the template exists.
   *
   * @param string $id
   *   Machine name (ID) of the template.
   *
   * @return bool
   *   Returns true, if the template exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $storage = $this->entityTypeManager->getStorage('inxmail_xml_template');
    return (bool) $storage->getQuery()->accessCheck(FALSE)->condition('id', $id)->execute();
  }

}
