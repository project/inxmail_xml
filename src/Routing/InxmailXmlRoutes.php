<?php

namespace Drupal\inxmail_xml\Routing;

use Drupal\inxmail_xml\Entity\TemplateEntity;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class InxmailXmlRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    // Find all templates.
    $query = \Drupal::entityQuery('inxmail_xml_template');
    $templateEntityIds = $query->execute();

    if (!empty($templateEntityIds)) {
      $routes = [];

      foreach ($templateEntityIds as $templateEntityId) {
        $templateEntity = TemplateEntity::load($templateEntityId);

        // Define a route for each template.
        $routes['inxmail_xml.template.' . $templateEntity->id()] = new Route(
        // Path to attach this route to:
          '/inxmail/' . $templateEntity->id() . '.xml',
          // Route defaults:
          [
            '_controller' => '\Drupal\inxmail_xml\Controller\InxmailXmlController::xml',
            '_title' => 'Inxmail XML'
          ],
          // Route requirements:
          [
            '_permission'  => 'access content',
          ]
        );
      }
      return $routes;
    }
  }

}
