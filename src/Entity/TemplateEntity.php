<?php

namespace Drupal\inxmail_xml\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the template entity.
 *
 * @ConfigEntityType(
 *   id = "inxmail_xml_template",
 *   label = @Translation("Inxmail XML template"),
 *   module = "inxmail_xml",
 *   config_prefix = "inxmail_xml_template",
 *   handlers = {
 *     "list_builder" = "Drupal\inxmail_xml\TemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\inxmail_xml\Form\TemplateForm",
 *       "edit" = "Drupal\inxmail_xml\Form\TemplateForm",
 *       "delete" = "Drupal\inxmail_xml\Form\TemplateDeleteForm",
 *     }
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "edit" = "/admin/config/services/inxmail-xml/templates/{inxmail_xml_template}",
 *     "delete" = "/admin/config/services/inxmail-xml/templates/{inxmail_xml_template}/delete",
 *     "collection" = "/admin/config/services/inxmail-xml/templates",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "template",
 *   }
 * )
 */
class TemplateEntity extends ConfigEntityBase implements TemplateEntityInterface {

  /**
   * Internal Drupal ID of the template entity.
   *
   * The ID.
   *
   * @var string
   */
  public $id = NULL;

  /**
   * Name of the template entity.
   *
   * @var string
   */
  public $name = NULL;

  /**
   * Code of the template.
   *
   * @var string
   */
  public $template = NULL;

}
