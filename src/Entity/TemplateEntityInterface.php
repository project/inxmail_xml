<?php

namespace Drupal\inxmail_xml\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for inxmail_xml templates.
 */
interface TemplateEntityInterface extends ConfigEntityInterface {
}
