CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 * Background information

INTRODUCTION
------------

The module enable to integrate Drupal contents into Inxmail newsletters.

More info:

 * For a full description of the module, visit
   [the project page](https://www.drupal.org/project/inxmail_xml).

 * To submit bug reports and feature suggestions, or to track changes, visit
   [the issue page](https://www.drupal.org/project/issues/inxmail_xml).

REQUIREMENTS
------------
There are no requirements.

RECOMMENDED MODULES
-------------------

There are no recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See the
   [docs](https://www.drupal.org/documentation/install/modules-themes/modules-8)
   for further information.

CONFIGURATION
-------------

You have to create an Inxmail XML template for every XML at: /admin/config/services/inxmail-xml/templates

Copy the XML in the template textarea and on the position, where the dynamical content should be displayed, use one of
the following insets:

### List of nodes

Embeds a list of links to nodes.

Example: {{"task":"list_of_nodes","content_type":"page","sort_field":"updated","sort_direction":"ASC","list_length":5}}

Variables:

| Variable       | Description                                                 | Default value |
|:---------------|:------------------------------------------------------------|:--------------|
| task           | The only possible value is : "list_of_nodes".               | -             |
| content_type   | Machine name of the content type, e.g.: "page".             | "article"     |
| sort_field     | Machine name of the field used for sorting, e.g.: "title".  | "created"     |
| sort_direction | Possible values: "ASC", "DESC"                              | "DESC"        |
| list_length    | Number of items in the list, e.g.: 15                       | 10            |

### List of nodes with day limits

This list is developed for a special purpose: newsletter, which has to be generated on selected days and only nodes
published in a specified range of days, should be included in the newsletter. The nodes are sorted under related
taxonomy terms.

Example: {{"task":"list_of_nodes_with_day_limits","content_type":"page","date_field":"field_published_on","start_day":10,"end_day":25,"taxonomy_fields":["field_tags","field_other_tags",]}}

Variables:

| Variable        | Description                                                   | Default value  |
|:----------------|:--------------------------------------------------------------|:---------------|
| task            | The only possible value is : "list_of_nodes_with_day_limits". | -              |
| content_type    | Machine name of the content type, e.g.: "page".               | "article"      |
| date_field      | Machine name of the date field, e.g.: "field_published_on".   | "field_date"   |
| start_day       | Possible values: 1 to 31                                      | 10             |
| end_day         | Possible values: 1 to 31                                      | 20             |
| taxonomy_fields | Array with machine names of taxonomy fields.                  | ["field_tags"] |

TROUBLESHOOTING
---------------

There are not any known issues. See:

 * [Issues](https://www.drupal.org/project/issues/inxmail_xml)
 * [Automated testing](#)
 * [PAReview](#)

FAQ
---

There are no answers or issues.

MAINTAINERS
-----------

Current maintainers:

 * [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)

BACKGROUND INFORMATION
-----------

 * [inxmail.com](https://www.inxmail.com)
 * [Inxmail: Integrating external content](https://help.inxmail.com/en/content/xpro/externen_content_einbinden.htm)
 * [Inxmail: Creating and setting up the data source](https://help.inxmail.com/en/content/xpro/datenquelle_anlegen_und_einrichten_1.htm)
